package jap.junit.integration;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Rule;
import org.junit.Test;

public class PersonDaoIT {

	@Rule
	public PersonRule personCreator = new PersonRule();

	@Test
	public void find() throws Exception {
		// Assemble
		Person expectedPerson = personCreator.createPerson("Dummy");
		PersonDao dao = new PersonDao();

		// Activate
		Person actualPerson = dao.find(expectedPerson.getName());

		// Assert
		assertThat(actualPerson, equalTo(expectedPerson));

	}
}
