package jap.junit.integration;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class PersonDaoOldSchoolIT {

	@Test
	public void find() throws Exception {
		// Assemble
		PersonDao dao = new PersonDao();
		Person expectedPerson = new Person("Dummy");
		dao.save(expectedPerson);

		try {
			// Activate
			Person actualPerson = dao.find(expectedPerson.getName());

			// Assert
			assertThat(actualPerson, equalTo(expectedPerson));
		} finally {
			// Cleanup
			dao.delete(expectedPerson);
		}
	}
}
