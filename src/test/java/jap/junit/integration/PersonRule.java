package jap.junit.integration;

import java.util.ArrayList;
import java.util.List;

import org.junit.rules.ExternalResource;

public class PersonRule extends ExternalResource {

	private List<Person> persons = new ArrayList<>();

	@Override
	protected void after() {
		System.out.println("Cleaning up");
		for (Person p : persons) {
			delete(p);
		}
	}

	private void delete(Person p) {
		System.out.println(String.format("Delete %s from database", p));
		new PersonDao().delete(p);
	}

	public Person createPerson(String name) {
		System.out.println(String.format("Create %s in database", name));
		Person p = new Person(name);
		persons.add(p);
		new PersonDao().save(p);
		return p;
	}

}
