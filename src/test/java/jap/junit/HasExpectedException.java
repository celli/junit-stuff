package jap.junit;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import static org.hamcrest.core.Is.*;
import static org.hamcrest.core.StringStartsWith.*;
 public class HasExpectedException {
 	@Rule
 	public ExpectedException thrown= ExpectedException.none();

 	@Test
 	public void throwsNothing() {
 		// no exception expected, none thrown: passes.
     }

 	@Test
 	public void throwsNullPointerException() {
 		thrown.expect(NullPointerException.class);
 		throw new NullPointerException();
     }

 	@Test
 	public void throwsNullPointerExceptionWithMessage() {
 		thrown.expect(NullPointerException.class);
 		thrown.expectMessage("happened?");
 		thrown.expectMessage(startsWith("What"));
 		throw new NullPointerException("What happened?");
     }

 	@Test
 	public void throwsIllegalArgumentExceptionWithMessageAndCause() {
 		NullPointerException expectedCause = new NullPointerException();
 		thrown.expect(IllegalArgumentException.class);
 		thrown.expectMessage("What");
 		thrown.expectCause(is(expectedCause));
 		
 		throw new IllegalArgumentException("What happened?", expectedCause);
     }
 }
