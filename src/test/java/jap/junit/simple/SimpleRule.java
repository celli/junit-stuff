package jap.junit.simple;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

public class SimpleRule implements TestRule {
	
	public Statement apply(final Statement base, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
            	System.out.println("evaluate start");
            	base.evaluate();
            	System.out.println("evaluate end");
            }
        };
	}
}
