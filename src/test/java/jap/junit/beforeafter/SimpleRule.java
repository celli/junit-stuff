package jap.junit.beforeafter;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

public class SimpleRule implements TestRule {

	private String name;

	public SimpleRule(String name) {
		this.name = name;
	}

	public Statement apply(final Statement base, final Description description) {
		return new Statement() {
			@Override
			public void evaluate() throws Throwable {
				System.out.println(name + ": evaluate start");
				base.evaluate();
				System.out.println(name + ":evaluate end");
			}
		};
	}
}
