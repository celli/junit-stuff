package jap.junit;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class HasGlobalTimeout {

	@Rule
	public Timeout globalTimeout = new Timeout(20);

	@Test
	public void testInfiniteLoop1() {
		System.out.println("Test1");
		for (;;) {
		}
	}

	@Test
	public void testInfiniteLoop2() {
		System.out.println("Test2");
		for (;;) {
		}
	}
	
	@Test(timeout=20)
	public void testInfiniteLoop3() {
		System.out.println("Test3");
		for (;;) {
		}
	}
}