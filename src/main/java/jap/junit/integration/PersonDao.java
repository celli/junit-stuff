package jap.junit.integration;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PersonDao {

	private static final Logger LOG = LoggerFactory.getLogger(PersonDao.class);

	private static Map<String, Person> db = new HashMap<>();

	public Person find(String name) {
		LOG.info("find {}", name);
		return db.get(name);
	}

	public void delete(Person p) {
		LOG.info("delete {}", p);
		if (p != null) {
			db.remove(p.getName());
		}
	}

	public void save(Person p) {
		LOG.info("save {}", p);
		if (p != null) {
			db.put(p.getName(), p);
		}
	}

}
